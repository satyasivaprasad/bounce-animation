package com.photoslib.siva.myphotoslib.internal.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.photoslib.siva.myphotoslib.R;
import com.photoslib.siva.myphotoslib.internal.entity.Item;
import com.photoslib.siva.myphotoslib.internal.entity.SelectionSpec;
import com.photoslib.siva.myphotoslib.internal.model.SelectedItemCollection;
import com.photoslib.siva.myphotoslib.internal.ui.adapter.PhotoFeedViewAdapter;

import java.util.List;

public class PhotoFeedViewScreen extends AppCompatActivity {

    public static final String EXTRA_DEFAULT_BUNDLE = "extra_default_bundle";
    public static final String EXTRA_RESULT_BUNDLE = "extra_result_bundle";
    public static final String EXTRA_RESULT_APPLY = "extra_result_apply";
    private static final int THUMBNAIL_SIZE = 350;
    private Bundle dataBundle;
    private ImageView sample;
    private List<Item> selectedItems;
    private static final int MAX_DIMENSION_FOR_UPLOAD = 800;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(SelectionSpec.getInstance().themeId);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_feed_view_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("Preview");

        dataBundle = getIntent().getBundleExtra(EXTRA_DEFAULT_BUNDLE);
        selectedItems = dataBundle.getParcelableArrayList(SelectedItemCollection.STATE_SELECTION);

        PhotoFeedViewAdapter photoFeedViewAdapter = new PhotoFeedViewAdapter(this);
        photoFeedViewAdapter.addAll(selectedItems);

        RecyclerView photoFeedRecyclerView = findViewById(R.id.selectedPhotosFeedRecycler);
        photoFeedRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        photoFeedRecyclerView.setAdapter(photoFeedViewAdapter);
        photoFeedViewAdapter.notifyDataSetChanged();


        FloatingActionButton fab = findViewById(R.id.send);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBackResult(true);
            }
        });

    }

    protected void sendBackResult(boolean apply) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_RESULT_BUNDLE, dataBundle);
        intent.putExtra(EXTRA_RESULT_APPLY, apply);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

}
