package com.photoslib.siva.myphotoslib.bounceanimation;

import android.animation.ValueAnimator;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.photoslib.siva.myphotoslib.R;

public class BounceActivity extends AppCompatActivity {

    private static final String LOG_TAG = BounceActivity.class.getCanonicalName();
    MediaPlayer mPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bounce_activity_main);

        setupAmplitudeSeekBar();
        setupFrequencySeekBar();
        setupDurationVar();

        animateButton();
    }

    public void onDestroy() {
        // Stop the sound
        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer = null;
        }

        super.onDestroy();
    }

    void updateTextViewValue(TextView textView, String prefix, double value) {
        String text = String.format("%s: %.2f", prefix, value);
        textView.setText(text);
    }

    double getSeekBarValue(SeekBar seekBar, double step) {
        return ((double)seekBar.getProgress() + 1.0) / ( 1.0 / step);
    }

    public void didTapPlayButton(View view) {
        animateButton();
    }

    void animateButton() {
//        ValueAnimator animator = ValueAnimator.ofInt(0, bottomNavigationHeight);
//        animator.setInterpolator(new DecelerateInterpolator());
//
//        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            @Override
//            public void onAnimationUpdate(ValueAnimator animation) {
//
//            }
//        });
//        animator.addUpdateListener((animation) ->
//        {
//            Integer value = (Integer) animation.getAnimatedValue();
//
//            // Shift the navigation bar off the screen
//            bottomNavigationView.setTranslationY(value);
//
//            // Shrink the margin at the same rate
//            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) contentView.getLayoutParams();
//            params.setMargins(0, 0, 0, bottomNavigationHeight - value);
//            contentView.setLayoutParams(params);
//        });
//        animator.setDuration(150);
//        animator.start();
//
//
//        // Load the animation
//        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
////        double animationDuration = getDurationValue() * 1000;
//
//        double animationDuration = 4000;
//
//        myAnim.setDuration((long)animationDuration);
//
//        // Use custom animation interpolator to achieve the bounce effect
//
//        Log.v(LOG_TAG, "Amplitude " + getAmplitudeValue() + " Frequency " + getFrequencyValue());
////        CustomBounceInterpolator interpolator = new CustomBounceInterpolator(getAmplitudeValue(), getFrequencyValue());
//
//        CustomBounceInterpolator interpolator = new CustomBounceInterpolator(0.1, 14);
//
//        myAnim.setInterpolator(interpolator);
//
//        // Animate the button
//        Button button = (Button)findViewById(R.id.play_button);
//        button.startAnimation(myAnim);
//        playSound();
//
//        // Run button animation again after it finished
//        myAnim.setAnimationListener(new Animation.AnimationListener(){
//            @Override
//            public void onAnimationStart(Animation arg0) {}
//
//            @Override
//            public void onAnimationRepeat(Animation arg0) {}
//
//            @Override
//            public void onAnimationEnd(Animation arg0) {
//                animateButton();
//            }
//        });
    }

    void playSound() {
        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.reset();
        }

        mPlayer = MediaPlayer.create(BounceActivity.this, R.raw.bubble);
        mPlayer.start();
    }

    // Duration controls
    // ---------------

    void setupDurationVar() {
        final SeekBar seekBar = findViewById(R.id.seek_bar_duration);
        seekBar.setProgress(19);
        updateDurationLabel();

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                animateButton();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                updateDurationLabel();
            }
        });
    }

    void updateDurationLabel() {
        TextView textView = (TextView) findViewById(R.id.text_view_duration);
        updateTextViewValue(textView, "Duration", getDurationValue());
    }

    double getDurationValue() {
        final SeekBar seekBar = findViewById(R.id.seek_bar_duration);
        return getSeekBarValue(seekBar, 0.1);
    }


    // Amplitude controls
    // ---------------

    void setupAmplitudeSeekBar() {
        final SeekBar dampingSeekBar = findViewById(R.id.seek_bar_amplitude);
        dampingSeekBar.setProgress(19);
        updateAmplitudeLabel();

        dampingSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                animateButton();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                updateAmplitudeLabel();
            }
        });
    }

    void updateAmplitudeLabel() {
        TextView textViewDamping = findViewById(R.id.text_view_amplitude);
        updateTextViewValue(textViewDamping, "Amplitude", getAmplitudeValue());
    }

    double getAmplitudeValue() {
        final SeekBar dampingSeekBar = findViewById(R.id.seek_bar_amplitude);
        return getSeekBarValue(dampingSeekBar, 0.01);
    }


    // Frequency controls
    // ---------------

    void setupFrequencySeekBar() {
        final SeekBar seekBar = findViewById(R.id.seek_bar_frequency);
        seekBar.setProgress(39);
        updateFrequencyLabel();

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                animateButton();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                updateFrequencyLabel();
            }
        });
    }

    void updateFrequencyLabel() {
        TextView textView = findViewById(R.id.text_view_frequency);
        updateTextViewValue(textView, "Frequency", getFrequencyValue());
    }

    double getFrequencyValue() {
        final SeekBar seekBar = findViewById(R.id.seek_bar_frequency);
        return getSeekBarValue(seekBar, 0.5);
    }
}
