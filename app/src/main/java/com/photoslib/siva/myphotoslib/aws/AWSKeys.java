package com.photoslib.siva.myphotoslib.aws;

import com.amazonaws.regions.Regions;

public class AWSKeys {
    protected static final String COGNITO_POOL_ID = "us-east-1:fd4619a7-7304-4595-a09b-9b247d01f29b";
    protected static final Regions MY_REGION = Regions.US_EAST_1; // WHAT EVER REGION IT MAY BE, PLEASE CHOOSE EXACT
    public static final String BUCKET_NAME = "magic-pic";

}
