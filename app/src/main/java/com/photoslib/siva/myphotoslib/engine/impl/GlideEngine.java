package com.photoslib.siva.myphotoslib.engine.impl;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.photoslib.siva.myphotoslib.engine.ImageEngine;


/**
 * {@link ImageEngine} implementation using Glide.
 */

public class GlideEngine implements ImageEngine {

    @Override
    public void loadThumbnail(Context context, int resize, Drawable placeholder, ImageView imageView, Uri uri) {
        Glide.with(context)
                .load(uri).apply(new RequestOptions().placeholder(placeholder).override(resize, resize).centerCrop())
                .into(imageView);
    }

    @Override
    public void loadGifThumbnail(Context context, int resize, Drawable placeholder, ImageView imageView,
                                 Uri uri) {
        Glide.with(context)
                .load(uri).apply(new RequestOptions().placeholder(placeholder).override(resize, resize).centerCrop())
                .into(imageView);
    }

    @Override
    public void loadImage(Context context, int resizeX, int resizeY, ImageView imageView, Uri uri) {
        Glide.with(context)
                .load(uri).apply(new RequestOptions().override(resizeX, resizeY).fitCenter().priority(Priority.HIGH))
                .into(imageView);
    }

    @Override
    public void loadGifImage(Context context, int resizeX, int resizeY, ImageView imageView, Uri uri) {
        Glide.with(context)
                .load(uri).apply(new RequestOptions().override(resizeX, resizeY).fitCenter().priority(Priority.HIGH))
                .into(imageView);
    }

    @Override
    public void loadImage(Context context, int resize, ImageView imageView, Uri uri) {
        Glide.with(context)
                .load(uri).apply(new RequestOptions().override(resize, resize).centerCrop().priority(Priority.HIGH))
                .into(imageView);
    }

    @Override
    public boolean supportAnimatedGif() {
        return true;
    }

}
