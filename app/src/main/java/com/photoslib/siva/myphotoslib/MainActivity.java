package com.photoslib.siva.myphotoslib;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.photoslib.siva.myphotoslib.engine.impl.GlideEngine;
import com.photoslib.siva.myphotoslib.internal.entity.CaptureStrategy;
import com.photoslib.siva.myphotoslib.internal.utils.PhotoMetadataUtils;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_CHOOSE = 23;
    private List<Uri> selectedMediaUris;
    private List<String> selectedMediaPaths;
    private Button letsRockBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        letsRockBtn = (Button) findViewById(R.id.letsRockBtn);

        letsRockBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopMediaPicker.from(MainActivity.this)
                        .choose(MimeType.ofImage(), false)
                        .countable(true)
                        .capture(true)
                        .captureStrategy(
                                new CaptureStrategy(true, "com.photoslib.siva.myphotoslib"))
                        .maxSelectable(32)
                        .gridExpectedSize(
                                getResources().getDimensionPixelSize(R.dimen.grid_expected_size))
                        .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                        .thumbnailScale(0.85f)
                        .showSingleMediaType(true)
                        .imageEngine(new GlideEngine())
                        .forResult(REQUEST_CODE_CHOOSE);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CHOOSE && resultCode == RESULT_OK) {
            selectedMediaUris = PopMediaPicker.obtainResult(data);
            selectedMediaPaths = PopMediaPicker.obtainPathResult(data);

            ContentResolver resolver = this.getContentResolver();
            Drawable drawableArray[] = new Drawable[selectedMediaUris.size()];
            for (int i = 0; i < selectedMediaUris.size(); i++) {
                Drawable result = PhotoMetadataUtils.getBitmapFromUri(resolver, selectedMediaUris.get(i));
                drawableArray[i] = result;
            }

            for (int i = 0; i < drawableArray.length / 2; i++) {
                Drawable temp = drawableArray[i];
                drawableArray[i] = drawableArray[drawableArray.length - 1 - i];
                drawableArray[drawableArray.length - 1 - i] = temp;
            }

            LayerDrawable layerDraw = new LayerDrawable(drawableArray);
            layerDraw.setLayerInset(0, 45, 25, 1, 1);
            layerDraw.setLayerInset(1, 35, 1, 25, 25);//set offset of 2 layer
//            layerDraw.setLayerInset(2, 25, 1, 45, 45);//set offset for third layer
            letsRockBtn.setBackground(layerDraw);
        }
    }

}
