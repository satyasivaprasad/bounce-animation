package com.photoslib.siva.myphotoslib

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.animation.Animation
import android.view.animation.Animation.AnimationListener
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.photoslib.siva.myphotoslib.bounceanimation.CustomBounceInterpolator
import kotlinx.android.synthetic.main.activity_slide_animation_screen.*
import kotlinx.android.synthetic.main.content_slide_animation_screen.*


class SlideAnimationScreen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_slide_animation_screen)
        setSupportActionBar(toolbar)

        fab.setOnClickListener {
            animateLaunch()
        }
    }

    private fun bounceAnimation() {
        val slideUpAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_up_animation)
        for_scale.startAnimation(slideUpAnimation)
        for_scale.setVisibility(View.VISIBLE);
        val interpolator = CustomBounceInterpolator(0.06, 15.0)
        slideUpAnimation.interpolator = interpolator
        for_scale.startAnimation(slideUpAnimation);
    }

    private fun slideFromRightToLeftAnimation() {
        val slideToLeft = AnimationUtils.loadAnimation(this, R.anim.slide_to_left)
        otp_view.startAnimation(slideToLeft)
        otp_view.setVisibility(View.VISIBLE);
        val interpolator = CustomBounceInterpolator(0.06, 15.0)
        slideToLeft.interpolator = interpolator
        otp_view.startAnimation(slideToLeft);
    }
    private fun animateLaunch() {
        bhoom_btn.setOnClickListener {
            slideFromRightToLeftAnimation()
        }
        val parentLayout: RelativeLayout = findViewById(R.id.parent)
        parentLayout.visibility = View.VISIBLE
        val widget: LinearLayout = findViewById(R.id.widget)
        val fadeInAnimation = AnimationUtils.loadAnimation(this, android.R.anim.fade_in)
        val slideUpWidgetAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_up_widget)
        slideUpWidgetAnimation.duration = 400
        slideUpWidgetAnimation.setAnimationListener(object : AnimationListener {
            override fun onAnimationStart(animation: Animation) {
                widget.visibility = View.VISIBLE
                bounceAnimation()
            }

            override fun onAnimationEnd(animation: Animation) {

            }

            override fun onAnimationRepeat(animation: Animation) {}
        })
        fadeInAnimation.duration = 300
        fadeInAnimation.fillAfter = true
        fadeInAnimation.setAnimationListener(object : AnimationListener {
            override fun onAnimationStart(animation: Animation) {
                widget.startAnimation(slideUpWidgetAnimation)
            }

            override fun onAnimationEnd(animation: Animation) {}
            override fun onAnimationRepeat(animation: Animation) {}
        })
        parentLayout.startAnimation(fadeInAnimation)
    }
}
