package com.photoslib.siva.myphotoslib.internal.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.photoslib.siva.myphotoslib.R;
import com.photoslib.siva.myphotoslib.internal.entity.Item;
import com.photoslib.siva.myphotoslib.internal.entity.SelectionSpec;
import com.photoslib.siva.myphotoslib.internal.utils.PhotoMetadataUtils;

import java.util.ArrayList;
import java.util.List;

public class PhotoFeedViewAdapter extends RecyclerView.Adapter<PhotoFeedViewAdapter.PhotoFeedViewHolder> {

    private final Context context;
    private Activity activity;
    private ArrayList<Item> selectedItems = new ArrayList<>();

    public PhotoFeedViewAdapter(Activity activity) {
        this.activity = activity;
        this.context = activity.getBaseContext();
    }

    @Override
    public PhotoFeedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater != null ? inflater.inflate(R.layout.photo_feedview_item, null) : null;
        return new PhotoFeedViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PhotoFeedViewHolder holder, int position) {
        Item item = selectedItems.get(position);
        Point size = PhotoMetadataUtils.getBitmapSize(item.getContentUri(), activity);
        if (item.isGif()) {
            SelectionSpec.getInstance().imageEngine.loadGifImage(context, size.x, size.y, holder.roundedRectangleImageView,
                    item.getContentUri());
        } else {
//            SelectionSpec.getInstance().imageEngine.loadImage(activity.getBaseContext(), size.x, size.y, holder.roundedRectangleImageView,
//                    item.getContentUri());
            SelectionSpec.getInstance().imageEngine.loadImage(context, size.x, context.getResources().getDimensionPixelSize(R
                            .dimen.feed_grid_size), holder.roundedRectangleImageView,
                    item.getContentUri());
        }
    }

    @Override
    public int getItemCount() {
        return selectedItems.size();
    }

    public Item getMediaItem(int position) {
        return selectedItems.get(position);
    }

    public void addAll(List<Item> items) {
        selectedItems.addAll(items);
    }

    class PhotoFeedViewHolder extends RecyclerView.ViewHolder {
        ImageView roundedRectangleImageView;

        PhotoFeedViewHolder(View itemView) {
            super(itemView);
            roundedRectangleImageView = itemView.findViewById(R.id.photo_item);
        }
    }
}
